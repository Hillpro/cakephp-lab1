<!-- File: src/Template/Articles/view.ctp -->

<h1><?= h($article->title) ?></h1>
<p><?= h($article->body) ?></p>
<p><b>Tags:</b> <?= h($article->tag_string) ?></p>
<p><small>Created: <?= h($article->created->format(DATE_RFC850)) ?></small></p>
<p><?= $this->Html->link('Edit', ['action' => 'edit', $article->slug]) ?></p>

<?php
    echo $this->Form->create($comment);
    echo $this->Form->control('comment');
    echo $this->Form->button(__('Save Comment'));
    echo $this->Form->end();
?>

<table>
    <tr>
        <th>Comment</th>
        <th>Created</th>
        <th>Email</th>
    </tr>

    <!-- Here is where we iterate through our $articles query object, printing out article info -->

    <?php foreach ($article->comments as $comment): ?>
    <tr>
        <td>
        <?= h($comment->comment) ?>
        </td>
        <td>
            <?= h($comment->created->format(DATE_RFC850)) ?>
        </td>
        <td>
            <?= $this->Html->link(h($comment->user->email), ['controller' => 'Users', 'action' => 'view', $comment->user_id]) ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
